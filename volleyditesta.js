//file js

function drawBox() {             
    contesto.fillstyle = this.fillstyle;                   
    contesto.fillRect(this.sx, this.sy, this.swidth, this.sheight); 
}      

function rectBox(sx, sy, swidth, sheight, stylestring)   {             
    this.sx = sx;             
    this.sy = sy;             
    this.swidth = swidth;             
    this.sheight = sheight;             
    this.fillstyle = stylestring;             
    this.draw = drawBox; 
}   

function disegnaScena() {             
    var i;             
    // contesto.clearRect(myBall.sxOld - 1, myBall.syOld - 1,52,52);             
    // contesto.clearRect(myLeftPlayer.sxOld - 10, myLeftPlayer.syOld - 30, 80,120);             
    // contesto.clearRect(myRightPlayer.sxOld - 10, myRightPlayer.syOld - 30, 80,120);             
    // contesto.clearRect(10, 15, 80,20);                             
    contesto.fillStyle = "rgb(0,0,0)";             
    
    rete.draw();

    // for (i = 0; i < oggetti.length; i ++){                         
    //     oggetti[i].draw();             
    // }                 
}

function gameLoop() {   
                       
    //if (!game.pause) {
        //salvo le posizioni correnti dei giocatori e della palla                           
        // myLeftPlayer.sxOld = myLeftPlayer.sx;                           
        // myLeftPlayer.syOld = myLeftPlayer.sy;    
        //                        
        // myRightPlayer.sxOld = myRightPlayer.sx;                           
        // myRightPlayer.syOld = myRightPlayer.sy;   
        //                         
        // myBall.sxOld = myBall.sx;                           
        // myBall.syOld = myBall.sy;   
                                
        // acquisizioneInput();                           
        // dinamicaGiocatori();                           
        // dinamicaPalla();                           
        disegnaScena();             
    //} 

}

function init(){
    //se esiste il canvas, se non esiste il canvas, canvas è uguale a null
    if (canvas.getContext) {                         

        contesto = canvas.getContext("2d");    
        canvasWidth = canvas.width;                           
        canvasHeight = canvas.height;     
                       
        rete = new rectBox( (canvasWidth / 2) - 1, canvasHeight - 80, 5, 200, "#64C864" );   

        setInterval(gameLoop, intervallo);             
    }
}

var canvas = document.getElementById("canvasvolley"); 

var canvasWidth; 
var canvasHeight; 

var sprite1 = new Image();     
sprite1. src = "./sprites/walk1.png"; 
// gambe animate 

var intervallo = 18;

init();